// Loops

// While loop



// let count = 5; // number of iteration, how many times we repeat our code

/*while (condition){  //condition- evaluates if it is true or false; if true loop will start and continue iteration;if false loop will stop
	block of code   -- wil be repeated by loop
	counter for iteration -- reason of our continuous loop/iteration
} */

// sample

let count =5;

while (count !== 0){  // condition- if the count value is not equal to 0
	console.log("Sylvan");
	count--; // will decrement by 1
}

/*print number 1-5 using while*/

let number =1;

while (number<=5){
	console.log(number);
	number++
}

let fruits = ['Banana', 'Mango']

let indexNumber = 0; //we will use this variable as reference to the index number of our given array

while (indexNumber <= 1 ){
	console.log(fruits[indexNumber]);
	indexNumber++;
}

let mobilePhones = ['Galaxy S21', 'Iphone 13 Pro', 'Xiaomi 11T', 'Realme C', 'Huawei Nova 8', 'Pixel 5', 'Asus ROG 6', 'Nokia', 'Cherry Mobile'];

console.log(mobilePhones.length);
console.log(mobilePhones.length -1); //will give us last index position of an element in an array
console.log(mobilePhones[mobilePhones.length -1]); //get the last element of an array

let indexNumberForMobile = 0;

while(indexNumberForMobile <= mobilePhones.length-1){
	console.log(mobilePhones[indexNumberForMobile]);
		indexNumberForMobile++;
}


/*Do-While Loop- do the statment once before going to the condition*/

let countA =1;

do {
	console.log('Juan');
	countA++;
}while(countA <= 6);


let countB = 6;

do{
	console.log(`Do While count${countB}`)
	countB--;
}while(countB == 7);

// vs

while(countB == 7){
	console.log(`While count ${countB}`)
	countB--;
}

// mini activity

let computerBrands = ['Apple Macbook Pro', 'HP NoteBook','Asus','Lenovo','Acer','dell','Huawei','razer']
let indexNumberA=0;

do{
	console.log(computerBrands[indexNumberA]);
	indexNumberA++;
}while (indexNumberA <= computerBrands.length-1);


// for loops
	//varfiable- the scope of the declared variable is within the for loop
	//condition
for (let count=5;count >=0 ; count--){
	console.log(count);
}

for (let indexNumberA=0; indexNumberA <= computerBrands.length-1 ; indexNumberA++){
	console.log(computerBrands[indexNumberA]);
}


let colors = ['red','green','blue','yellow','purple','white','black']


for (let indexColor=0; indexColor <= colors.length-1 ; indexColor++){
	console.log(colors[indexColor]);
}

//continue and break

let ages = [18,19,20,21,24,25];

for (let i = 0; i <= ages.length-1; i++){
	if(ages[i] == 21 || ages[i] == 18){
		continue;
	}
	console.log(ages[i]);

}



let studentNames = ['Den', 'Jayson', 'Marvin', 'Rommel'];
for (let i = 0;  i <= studentNames.length -1; i++) {
	if(studentNames[i] == "Jayson"){
		console.log(studentNames[i])
		break;
	}
}


//Coding Challenge - Scope (Loops, Continue and Break)
// You can add the solution under our s16/d1/index.js only, no need to create a separate folder for this. Kindly push your solutions once you are done
//Then link your activity gitlab repo under Boodle WD078-16

/*
Instructions:

1. Given the array 'adultAge', display only adult age range on the console.

-- the goal of this activity is to exclude values that are not in the range of adult age. Using the tool loops, continue or break, 
the students must display only the given sample output below on their console.
*/
let adultAge = [20, 23, 33, 27, 18, 19, 70, 15, 55, 63, 85, 12, 19];



for (let i=0; i <= adultAge.length-1; i++){
	if(adultAge[i] < 18){
		continue;
	}
	console.log(adultAge[i]);
}



// Instructions:

// 2. Given an array 'students' and a function searchStudent, create a solution that, once a function is invoked with a given name of student as its argument,
// 	the function will start to loop and search for the student on a given array and once there's found it will print it on the console and stop the execution of loop. 
  
//   -- the goal of this activity is to print only the value needed based on the argument given. Use loop, and continue or break
// */
let students = ['Gary', 'Amelie', 'Anne', 'Jazz', 'Preina', 'James', 'Kelly', 'Diane', 'Lucy', 'Vanessa', 'Kim', 'Francine'];

function searchStudent(studentName){
	for(let i=0; i <= students.length-1; i++ ){
		if(students[i] == studentName){
			console.log(studentName);
			break;
		}
	}



	//add solutions here
  //you can refer on our last discussion yesterday
}

searchStudent('Jazz'); //invoked function with a given argument 'Jazz'
searchStudent('Francine'); //testing function
searchStudent('Lucy'); //testing function

/*
	Sample output:
  
  Jazz
*/



























